package com.epam.javafundamentals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class InputHandler {
    public void validateInput() {                                                                                      //method to validate user's input
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));                             //buffered reader to read input
        boolean repeatInputRequest = true;
        while (repeatInputRequest) {                                                                                    //repeat until input is valid
            ArrayList<String> inputData = new ArrayList<>();                                                            //array list to store input
            try {
                System.out.println("Please enter a value (integer) of the matrix's size (n > 1), a[n][n]...");          //reading user's input
                String inputMatrixSize = inputReader.readLine();
                inputData.add(inputMatrixSize);
                System.out.println("Please enter an integer (M > 0). Matrix values will be chosen from the range (-M...M) randomly...");
                String inputMatrixValueRange = inputReader.readLine();
                inputData.add(inputMatrixValueRange);
                System.out.println("Please enter 0 if you want matrix rows to be sorted according to values in column (k)");
                System.out.println("Or enter 1 if you want matrix columns to be sorted according to values in row (k)...");
                String inputSortingMode = inputReader.readLine();
                inputData.add(inputSortingMode);
                System.out.println("Please enter a number (integer) of a row/column (k), according to which matrix will be sorted (1 <= k <= " + inputMatrixSize + ")...");
                String inputNumberOfSortingColumnOrRow = inputReader.readLine();
                inputData.add(inputNumberOfSortingColumnOrRow);
                System.out.println(String.format("%70s", " ").replace(' ', '-'));
                //checking if user's input is not empty
                if (!inputMatrixSize.equals("") && !inputMatrixValueRange.equals("") && !inputSortingMode.equals("") && !inputNumberOfSortingColumnOrRow.equals("")) {
                    boolean correctInput = true;
                    for (int i = 0; i < inputData.size(); i++) {                                                        //looping through entered data
                        try {
                            Integer.parseInt(inputData.get(i));                                                         //checking if entered parameter is an integer
                            if (i == 0 && Integer.parseInt(inputData.get(i)) < 1) {                                     //checking if matrix size is < 1
                                correctInput = false;
                                break;
                            } else if (i == 1 && Integer.parseInt(inputData.get(i)) < 1) {                             //checking if matrix values range is > 0
                                correctInput = false;
                                break;
                            }
                            //checking if user used 0 or 1 to choose between row wise or column wise sorting
                            else if (i == 2 && Integer.parseInt(inputData.get(i)) != 0 && Integer.parseInt(inputData.get(i)) != 1) {
                                correctInput = false;
                                break;
                            }
                            //checking if number of column/row according to which matrix is being sorted is in the correct range
                            else if (i == 3 && Integer.parseInt(inputData.get(i)) < 1 && Integer.parseInt(inputData.get(i)) > Integer.parseInt(inputData.get(0))) {
                                correctInput = false;
                                break;
                            }
                        } catch (NumberFormatException e) {
                            correctInput = false;
                            System.out.println("One of the entered parameters is not an integer!");
                            break;
                        }
                    }
                    if (correctInput) {                                                                                 //if input is valid then parse it to integers
                        repeatInputRequest = false;
                        int[] parsedData = new int[inputData.size()];
                        for (int i = 0; i < inputData.size(); i++) {                                                    //looping through entered data
                            parsedData[i] = Integer.parseInt(inputData.get(i));
                        }
                        processInput(parsedData);                                                                       //processing parsed data
                    } else {
                        System.out.println("Some of the entered parameters are not in the correct range! Please, try again...\n");
                    }
                } else {
                    System.out.println("One or all of entered values are empty! Please, try again...\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
        try {
            inputReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processInput(int[] parsedData) {                                                                      //method to process input
        System.out.println("Initial matrix: ");
        int[][] initialMatrix = generateRandomMatrixValues(parsedData[0], parsedData[1]);                               //generating random matrix values
        printMatrix(initialMatrix);
        System.out.println("-----------------------------------------");
        if (parsedData[2] == 0) {                                                                                       //checking sorting mode
            System.out.println("Matrix after sorting rows by column " + parsedData[3] + ":");
        } else {
            System.out.println("Matrix after sorting columns by row " + parsedData[3] + ":");
        }
        int[][] sortedMatrix = sortMatrix(initialMatrix, parsedData[2], parsedData[3] - 1);           //sorting matrix row/column wise
        printMatrix(sortedMatrix);
        System.out.println("-----------------------------------------");
        printSumOfPositiveElements(sortedMatrix);                                                                       //printing sum of first and second positive element in a row
        System.out.println("-----------------------------------------");
        System.out.println("Matrix after deleting all rows and columns which contain max matrix value:");
        printMatrix(deleteRowsAndColumnsWithMaxElement(sortedMatrix));
    }


    private int[][] generateRandomMatrixValues(int numberOfRowsAndColumns, int rangeOfMatrixValues) {                 //method to generate random matrix values
        int[][] matrix = new int[numberOfRowsAndColumns][numberOfRowsAndColumns];                                      //creating empty matrix
        Random randomGenerator = new Random();
        for (int row = 0; row < numberOfRowsAndColumns; row++) {
            for (int column = 0; column < numberOfRowsAndColumns; column++) {
                matrix[row][column] = randomGenerator.nextInt((2 * rangeOfMatrixValues) + 1) - rangeOfMatrixValues; //random value in range (-rangeOfMatrixValues to +rangeOfMatrixValues)
            }
        }
        return matrix;
    }

    private int[][] sortMatrix(int[][] matrix, int sortingMode, final int sortingRowOrColumn) {                       //method to sort matrix
        int[][] temporaryMatrix = new int[matrix.length][matrix.length];                                               //creating temporary matrix
        int[][] matrixToReturn = matrix.clone();                                                                        //creating matrix which will be returned (clone of initial matrix)
        if (sortingMode != 0) {                                                                                         //checking sorting mode
            for (int i = 0; i < matrix.length; i++) {                                                                  //if sorting mode is row wise, then make rows of temporaryMatrix equal to columns of initial matrix
                for (int j = 0; j < matrix[i].length; j++) {
                    temporaryMatrix[i][j] = matrixToReturn[j][i];
                }
            }
        } else {
            temporaryMatrix = matrixToReturn.clone();
        }

        Arrays.sort(temporaryMatrix, new Comparator<int[]>() {                                                         // Using built-in sort function Arrays.sort to sort matrix column wise
            @Override
            public int compare(int[] entry1, int[] entry2) {                                                          // Compare rows according to columns
                return Integer.compare(entry1[sortingRowOrColumn], entry2[sortingRowOrColumn]);
            }
        });

        if (sortingMode != 0) {                                                                                         //if sorting mode is row wise, then make columns of initial matrix equal to rows of temporaryMatrix
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    matrixToReturn[j][i] = temporaryMatrix[i][j];
                }
            }
            return matrixToReturn;
        } else {
            return temporaryMatrix;
        }

    }

    private void printMatrix(int[][] matrix) {                                                                        //method to print matrix
        for (int[] row : matrix) {                                                                                      //loop through each matrix element
            for (int matrixElement : row) System.out.printf("%-5d", matrixElement);
            System.out.println();
        }
    }

    private void printSumOfPositiveElements(int[][] matrix) {                                                         //method to print first and second positive matrix element
        for (int i = 0; i < matrix.length; i++) {                                                                       //looping through each row of a matrix
            int sumOfPositiveElements = 0;
            int firstPositiveElement = -1;
            int firstPositiveElementIndex = 0;
            int secondPositiveElement = -1;
            int secondPositiveElementIndex = 0;
            for (int j = 0; j < matrix[i].length; j++) {                                                                //looping through each element of a row
                if (matrix[i][j] > 0) {                                                                                 //checking if value is positive
                    firstPositiveElement = matrix[i][j];
                    firstPositiveElementIndex = j;
                    break;
                }
            }
            for (int j = firstPositiveElementIndex + 1; j < matrix[i].length; j++) {                                    //another loop through each element of row, starting from first positive element
                if (matrix[i][j] > 0) {                                                                                 //checking if value is positive
                    secondPositiveElement = matrix[i][j];
                    secondPositiveElementIndex = j;
                    break;
                }
            }
            if (firstPositiveElement != -1 && secondPositiveElement != -1) {                                            //if there are 2 positive elements in a row,
                for (int j = firstPositiveElementIndex + 1; j < secondPositiveElementIndex; j++) {                       //sum all elements between them
                    sumOfPositiveElements += matrix[i][j];
                }
            }
            if (firstPositiveElement == -1) {                                                                           //checking if there are no positive elements in a row
                System.out.println("row #" + (i + 1) + ". No positive elements in a row;");
            } else if (secondPositiveElement == -1) {                                                                   //checking if there are only one positive element in a row
                System.out.println("row #" + (i + 1) + ". Only one positive element in a row;");
            } else if (secondPositiveElementIndex == firstPositiveElementIndex + 1) {                                    //checking if there are no elements between first and second positive elements
                System.out.println("row #" + (i + 1) + ". No elements between first and second positive elements;");
            } else {
                System.out.println("row #" + (i + 1) + ". Elements: " + firstPositiveElement + ", " + secondPositiveElement + "; Sum: " + sumOfPositiveElements + ";");
            }
        }
    }

    private int[][] deleteRowsAndColumnsWithMaxElement(int[][] matrix) {                                              //method to delete rows and columns with max matrix element
        int[][] temporaryMatrix = matrix.clone();                                                                       //creating temporary matrix
        ArrayList<ArrayList<Integer>> arrayListToPrint = new ArrayList<>();
        for (int i = 0; i < temporaryMatrix.length; i++) {                                                              //converting arrays of ints to ArrayLists of Integers
            ArrayList<Integer> row = new ArrayList<>();
            for (int j = 0; j < temporaryMatrix[i].length; j++) row.add(j, temporaryMatrix[i][j]);
            arrayListToPrint.add(i, row);
        }
        Integer maxMatrixValue = 0;                                                                                     //looking for max value in matrix
        for (ArrayList<Integer> row : arrayListToPrint) {
            if (Collections.max(row) > maxMatrixValue) {
                maxMatrixValue = Collections.max(row);
            }
        }
        HashSet<Integer> setOfRowsToDelete = new HashSet<>();                                                           //creating set of rows indexes to delete
        HashSet<Integer> setOfColumnsToDelete = new HashSet<>();                                                        //creating set of columns indexes to delete
        for (int i = 0; i < arrayListToPrint.size(); i++) {                                                            //looking for rows and columns with max matrix value
            for (int j = 0; j < arrayListToPrint.get(i).size(); j++) {
                if (arrayListToPrint.get(i).get(j).equals(maxMatrixValue)) {
                    setOfRowsToDelete.add(i);
                    setOfColumnsToDelete.add(j);
                }
            }
        }
        Integer[] rowsToDelete = new Integer[setOfRowsToDelete.size()];
        Integer[] columnsToDelete = new Integer[setOfColumnsToDelete.size()];
        setOfRowsToDelete.toArray(rowsToDelete);                                                                        //converting set's of rows and columns which have to be deleted to arrays
        setOfColumnsToDelete.toArray(columnsToDelete);
        Arrays.sort(rowsToDelete);                                                                                      //sorting arrays with indexes of rows and columns to delete
        Arrays.sort(columnsToDelete);
        for (int row = rowsToDelete.length - 1; row >= 0; row--) {                                                      //deleting each row with the max value of a matrix
            arrayListToPrint.remove(rowsToDelete[row].intValue());
        }
        for (ArrayList<Integer> row : arrayListToPrint) {                                                               //deleting each column with the max value of a matrix
            for (int column = columnsToDelete.length - 1; column >= 0; column--) {
                row.remove(columnsToDelete[column].intValue());
            }
        }
        int numberOfColumns;
        if (!arrayListToPrint.isEmpty()) {                                                                              //checking if result matrix is not empty
            numberOfColumns = arrayListToPrint.get(0).size();
            int[][] matrixToPrint = new int[arrayListToPrint.size()][numberOfColumns];                                 //converting arrayLists to arrays
            for (int i = 0; i < matrixToPrint.length; i++) {
                for (int j = 0; j < matrixToPrint[i].length; j++) {
                    matrixToPrint[i][j] = arrayListToPrint.get(i).get(j);
                }
            }
            return matrixToPrint;
        } else {
            System.out.println("After deleting all rows and columns which contain max matrix value, resulting matrix has become empty!");
            return new int[0][0];
        }
    }

}





