package com.epam.javafundamentals;


public class FourthTask {
    public static void main(String[] args) {
        InputHandler inputHandler = new InputHandler();             //instantiating input handling class
        inputHandler.validateInput();                               //validating user's input, if valid, process it
    }
}
